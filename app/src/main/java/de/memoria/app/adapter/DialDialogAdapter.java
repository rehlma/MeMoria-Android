package de.memoria.app.adapter;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import de.memoria.app.R;

/**
 * Created by maikel on 30.10.14.
 */
public class DialDialogAdapter extends CursorAdapter {

    public DialDialogAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // when the view will be created for first time,
        // we need to tell the adapters, how each item will look
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.dial_dialog_item, parent, false);

        return retView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewPhoneNumber = (TextView) view.findViewById(R.id.dialDialogPhoneNumber);
        textViewPhoneNumber.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1))));

        String[] phoneType = context.getResources().getStringArray(R.array.phoneTyp);

        TextView textViewPhoneType = (TextView) view.findViewById(R.id.dialDialogPhoneType);
        textViewPhoneType.setText(
            phoneType[
                getTypeId(
                    Integer.parseInt(
                        cursor.getString(cursor.getColumnIndex(cursor.getColumnName(2)))
                    )
                ) - 1
            ]
        );
    }

    private int getTypeId (int type) {
        int typeId = 1;
        switch (type) {
            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                typeId = 1;
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                typeId = 2;
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                typeId = 3;
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME:
                typeId = 4;
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK:
                typeId = 5;
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_MAIN:
                typeId = 6;
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                typeId = 7;
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM:
                typeId = 8;
                break;
            case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
                typeId = 9;
                break;
        }
        return typeId;
    }
}
