package de.memoria.app.model;

/**
 * Created by maikel on 28.03.15.
 */
public class User {

    private static User user;

    public static User getInstance() {
        if (user == null) user = new User();
        return user;
    }

    private String expiresAt;
    private String name;

    public String getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
