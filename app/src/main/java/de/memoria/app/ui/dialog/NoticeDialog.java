package de.memoria.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.util.TypedValue;
import android.widget.EditText;
import de.memoria.app.R;

/**
 * Created by maikel on 30.10.14.
 */

public class NoticeDialog extends DialogFragment {

    public static final String EDIT_TEXT = "_EDIT_TEXT";
    public static final String EDIT_TEXT_ID = "_EDIT_TEXT_ID";
    public static final String ACTION = "_ACTION";
    public static final int ACTION_ADD = 0;
    public static final int ACTION_EDIT = 1;
    public static final int ACTION_DELETE = 2;

    /**
     * Implement this interface to receive a bundle
     * that contains the ACTION and the TEXT
     */
    public interface NoticeDialogListener {
        public void onNoticeDialogCallback(Bundle bundle);
    }

    public void setNoticeDialogListener(NoticeDialogListener callback) {
        this.callback = callback;
    }

    NoticeDialogListener callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.memoriaDialog);
        try {
            if (callback == null)
                callback = (NoticeDialogListener) getTargetFragment();
        } catch (Exception e) {
            throw new ClassCastException("Calling Fragment must implement onNoticeDialogCallback");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final EditText editText = new EditText(getActivity());
        int dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        editText.setPadding(dp,dp,dp,dp);
        editText.setSingleLine(false);
        editText.setMinLines(3);
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        final Bundle putBundle = new Bundle();

        String negativeButtonText = getResources().getString(R.string.dialog_generel_cancel);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(editText);
        builder.setTitle(R.string.dialog_generel_add);

        if(getArguments() != null) {
            putBundle.putAll(getArguments());

            if (putBundle.containsKey(EDIT_TEXT) && putBundle.containsKey(EDIT_TEXT_ID)) {
                editText.setText(putBundle.getString(EDIT_TEXT));
                builder.setTitle(R.string.dialog_generel_edit);
                negativeButtonText = getResources().getString(R.string.dialog_generel_delete);
            }
        }

        builder.setPositiveButton(R.string.dialog_generel_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String text = editText.getText().toString();

                if (text.trim().length() == 0) {
                    dismiss();
                } else {
                    if (putBundle.containsKey(EDIT_TEXT_ID)) {
                        putBundle.remove(EDIT_TEXT);
                        putBundle.putInt(ACTION, ACTION_EDIT);
                    } else
                        putBundle.putInt(ACTION, ACTION_ADD);

                    putBundle.putString(EDIT_TEXT, text);
                    callback.onNoticeDialogCallback(putBundle);
                }
            }
        });
        builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(putBundle.containsKey(EDIT_TEXT_ID)) {
                    putBundle.putInt(ACTION, ACTION_DELETE);
                    callback.onNoticeDialogCallback(putBundle);
                }
                else
                    dismiss();
            }
        });
        return builder.create();
    }
}
