package de.memoria.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import de.memoria.app.R;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by maikel on 30.10.14.
 */

public class CalendarDialog extends DialogFragment {

    public static final String CALENDAR_ID = "_CALENDAR_ID";
    public static final String CALENDAR_TITLE = "_CALENDAR_TITLE";
    public static final String CALENDAR_DATE = "_CALENDAR_DATE";
    public static final String ACTION = "_ACTION";
    public static final int ACTION_ADD = 0;
    public static final String CONTACT_NAME = "_CONTACT_NAME";
    public static final String CONTACT_URI = "_CONTACT_URI";
    public static final String CONTACT_ID = "_CONTACT_ID";

    /**
     * Implement this interface to receive a bundle
     * that contains the ACTION and the TEXT
     */
    public interface CalendarDialogListener {
        public void onCalendarDialogCallback(CalendarDialog CalendarDialog, Bundle bundle);
    }

    public void setCalendarDialogListener(CalendarDialogListener callback) {
        this.callback = callback;
    }

    CalendarDialogListener callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.memoriaDialog);
        try {
            if(callback == null)
                callback = (CalendarDialogListener) getTargetFragment();
        } catch (Exception e) {
            throw new ClassCastException("Calling Fragment must implement onCalendarDialogCallback");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final EditText editText = new EditText(getActivity());
        int dp = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        editText.setPadding(dp, dp, dp, dp);
        editText.setSingleLine();
        editText.setHint("Titel");
        final DatePicker datePicker = new DatePicker(getActivity());
        final TimePicker timePicker = new TimePicker(getActivity());
        timePicker.setIs24HourView(true);
        datePicker.setCalendarViewShown(false);

        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(editText);
        linearLayout.addView(datePicker);
        linearLayout.addView(timePicker);

        final Bundle putBundle = new Bundle();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(linearLayout);
        builder.setTitle(R.string.dialog_generel_add);

        if(getArguments() != null) {
            Bundle getBundle = new Bundle();
            getBundle.putAll(getArguments());
            if (getBundle.containsKey(CONTACT_URI)) {
                putBundle.putString(CONTACT_URI, getBundle.getString(CONTACT_URI));
            }
            if (getBundle.containsKey(CONTACT_ID)) {
                putBundle.putString(CONTACT_ID, getBundle.getString(CONTACT_ID));
            }

            if (getBundle.containsKey(CONTACT_NAME)) {
                editText.setText(getResources().getString(R.string.dialog_calendar_default_text, getBundle.getString(CONTACT_NAME)));
            }
            else if(getBundle.containsKey(CALENDAR_TITLE)) {
                editText.setText(getBundle.getString(CALENDAR_TITLE));
            }
        }

        builder.setPositiveButton(R.string.dialog_generel_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String text = editText.getText().toString();

                if (text.trim().length() == 0) {
                    dismiss();
                } else {
                    Calendar calendar = Calendar.getInstance(Locale.GERMANY);
                    calendar.set(Calendar.YEAR, datePicker.getYear());
                    calendar.set(Calendar.MONTH, datePicker.getMonth());
                    calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                    calendar.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                    calendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());
                    calendar.set(Calendar.SECOND, 0);

                    putBundle.putInt(ACTION, ACTION_ADD);
                    putBundle.putString(CALENDAR_TITLE, text);
                    putBundle.putLong(CALENDAR_DATE, calendar.getTimeInMillis());

                    callback.onCalendarDialogCallback(CalendarDialog.this, putBundle);
                }
            }
        });
        builder.setNegativeButton(R.string.dialog_generel_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                CalendarDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }
}
