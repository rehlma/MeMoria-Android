package de.memoria.app.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.*;
import android.widget.ListView;
import de.memoria.app.R;
import de.memoria.app.adapter.ReminderAdapter;
import de.memoria.app.database.DatabaseHelper;
import de.memoria.app.ui.dialog.ReminderDialog;

public class ReminderFragment extends ListFragment implements
        ReminderDialog.ReminderDialogListener {

    private ReminderAdapter mReminderAdapter;
    private DatabaseHelper databaseHelper;

    public static ReminderFragment newInstance() {
        final ReminderFragment fragment = new ReminderFragment();
        return fragment;
    }

    /**
     * Fragments require an empty constructor.
     */
    public ReminderFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if(getArguments() != null && getArguments().getBoolean(ReminderActivity.FROM_NOTIFICATION, false)) {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle(R.string.dialog_reminder_new);
            alertDialog.setNegativeButton(R.string.dialog_generel_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    return;
                }
            });
            alertDialog.setPositiveButton(R.string.dialog_generel_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    showReminderDialog(getArguments().getString(ReminderActivity.FROM_NOTIFICATION_TITLE));
                    return;
                }
            });
            alertDialog.create().show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflates the main layout to be used by this fragment
        View detailView = inflater.inflate(R.layout.reminder_fragment, container, false);
        return detailView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        databaseHelper = DatabaseHelper.getInstance(getActivity());
        databaseHelper.open(true);

        mReminderAdapter = new ReminderAdapter(
                getActivity().getApplicationContext(),
                databaseHelper.getReminder()
        );
        setListAdapter(mReminderAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        final Cursor cursor = mReminderAdapter.getCursor();
        cursor.moveToPosition(position);

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(R.string.dialog_reminder_delete_title);
        alertDialog.setNegativeButton(R.string.dialog_generel_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });
        alertDialog.setPositiveButton(R.string.dialog_generel_delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                databaseHelper.deleteReminder(cursor.getString(cursor.getColumnIndex(DatabaseHelper.REMINDER_TABLE_COLUMN_ID)));
                mReminderAdapter.changeCursor(databaseHelper.getReminder());
                return;
            }
        });
        alertDialog.create().show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_reminder:
                showReminderDialog("");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // Inflates the options menu for this fragment
        inflater.inflate(R.menu.reminder_menu, menu);
    }

    @Override
    public void onDestroy() {
        DatabaseHelper.getInstance(getActivity()).close();
        super.onDestroy();
    }

    private void showReminderDialog(String defaultText) {
        Bundle bundle = new Bundle();
        bundle.putString(ReminderDialog.REMINDER_TITLE, defaultText);
        ReminderDialog reminderDialog = new ReminderDialog();
        reminderDialog.setTargetFragment(this, 0);
        reminderDialog.setArguments(bundle);
        reminderDialog.show(getFragmentManager(), "reminderDialog");
    }

    @Override
    public void onReminderDialogCallback(ReminderDialog reminderDialog, Bundle bundle) {
        reminderDialog.dismiss();
        databaseHelper.insertReminder(
                bundle.getString(ReminderDialog.REMINDER_TITLE),
                bundle.getString(ReminderDialog.REMINDER_DATE)
         );
        mReminderAdapter.changeCursor(databaseHelper.getReminder());
    }
}
