/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.memoria.app.ui;

import android.content.*;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import de.memoria.app.R;
import de.memoria.app.adapter.CalenderAdapter;
import de.memoria.app.adapter.NoticeAdapter;
import de.memoria.app.database.DatabaseHelper;
import de.memoria.app.ui.dialog.CalendarDialog;
import de.memoria.app.ui.dialog.NoticeDialog;
import de.memoria.app.ui.dialog.ReminderDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class DetailFragment extends Fragment implements
        AdapterView.OnItemClickListener,
        NoticeDialog.NoticeDialogListener,
        CalendarDialog.CalendarDialogListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    public static final String EXTRA_CONTACT_URI = "EXTRA_CONTACT_URI";
    public static final String SELECTED_MODE = "SELECTED_MODE";
    public static final int MODE_NOTICE = 0;
    public static final int MODE_CALENDAR = 1;
    private int selectedMode;

    private Uri mContactUri;

    private ListView mDetailListView;
    private TextView mDetailListViewTitle;
    private Button mDetailButtonNotice;
    private Button mDetailButtonCalendar;

    private NoticeAdapter mNoticeAdapter;
    private CalenderAdapter mCalenderAdapter;
    private DatabaseHelper databaseHelper;

    private String mContactID;
    private String mContactName;
    private int mCalendarId;

    public void setContact(Uri contactLookupUri) {
        mContactUri = contactLookupUri;
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        mCalendarId = settings.getInt(ContactsListFragment.CALENDAR_ID, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View detailView = inflater.inflate(R.layout.detail_fragment, container, false);

        mDetailListViewTitle = (TextView) detailView.findViewById(R.id.detailListViewTitle);

        mDetailListView = (ListView) detailView.findViewById(R.id.detailListView);
        mDetailListView.setEmptyView(detailView.findViewById(R.id.detailListViewEmpty));
        mDetailListView.setOnItemClickListener(this);

        mDetailButtonNotice = (Button) detailView.findViewById(R.id.detailButtonNotice);
        mDetailButtonNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMode(MODE_NOTICE);
            }
        });
        mDetailButtonCalendar = (Button) detailView.findViewById(R.id.detailButtonCalendar);
        mDetailButtonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMode(MODE_CALENDAR);
            }
        });

        return detailView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);

        if (savedInstanceState == null) {
            if (getArguments() != null) {
                setContact(Uri.parse(getArguments().getString(EXTRA_CONTACT_URI)));
                if (getArguments().containsKey(SELECTED_MODE))
                    selectedMode = getArguments().getInt(SELECTED_MODE);
            }
        } else {
            setContact((Uri) savedInstanceState.getParcelable(EXTRA_CONTACT_URI));
            selectedMode = MODE_NOTICE;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        databaseHelper = DatabaseHelper.getInstance(getActivity().getApplicationContext());
        databaseHelper.open(true);
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EXTRA_CONTACT_URI, mContactUri);
    }

    @Override
    public void onCalendarDialogCallback(CalendarDialog CalendarDialog, Bundle bundle) {
//        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getActivity().getApplicationContext());
//        databaseHelper.open();

        ContentResolver cr = getActivity().getContentResolver();

        Calendar beginTime = Calendar.getInstance();
        beginTime.setTimeInMillis(bundle.getLong(CalendarDialog.CALENDAR_DATE));

        // Set event values
        ContentValues eventValues = new ContentValues();
        eventValues.put(CalendarContract.Events.TITLE, bundle.getString(CalendarDialog.CALENDAR_TITLE));
        eventValues.put(Events.DTSTART, beginTime.getTimeInMillis());
        eventValues.put(Events.DTEND, beginTime.getTimeInMillis());
        eventValues.put(Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
        eventValues.put(Events.CALENDAR_ID, mCalendarId);
        Uri insertUri = cr.insert(Events.CONTENT_URI, eventValues);

        int eventID = Integer.parseInt(insertUri.getLastPathSegment());

        // Add Contact to event;
        databaseHelper.insertEvents(mContactID, eventID);

        // Add reminder to event
        ContentValues reminderValues = new ContentValues();
        reminderValues.put(Reminders.MINUTES, 15);
        reminderValues.put(Reminders.EVENT_ID, eventID);
        reminderValues.put(Reminders.METHOD, Reminders.METHOD_ALERT);
        cr.insert(Reminders.CONTENT_URI, reminderValues);

        // Add reminder for event
        Calendar reminderDate = beginTime;
        reminderDate.add(Calendar.DAY_OF_MONTH, 1);
        String date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")).format(reminderDate.getTimeInMillis());
//        databaseHelper.open();
        databaseHelper.insertReminder(
                bundle.getString(CalendarDialog.CALENDAR_TITLE),
                date
        );
//        DatabaseHelper.getInstance(getActivity()).close();

        // Open Event
        Uri editUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
        Intent calendarIntent = new Intent(Intent.ACTION_VIEW)
                .setData(editUri);
        startActivity(calendarIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_detail_add:
                switch (selectedMode) {
                    case MODE_NOTICE:
                        NoticeDialog noticeDialog = new NoticeDialog();
                        noticeDialog.setTargetFragment(this, 0);
                        noticeDialog.show(getFragmentManager(), "noticeDialog");
                        break;
                    case MODE_CALENDAR:
                        if(mCalendarId == 0) break;
                        Log.d("mCalendarId", mCalendarId + "");

                        Bundle bundle = new Bundle();
                        CalendarDialog calendarDialog = new CalendarDialog();
                        bundle.putString(ReminderDialog.CONTACT_NAME, mContactName);
                        calendarDialog.setArguments(bundle);
                        calendarDialog.setTargetFragment(DetailFragment.this, 0);
                        calendarDialog.show(getFragmentManager(), "calendarDialog");
                        break;
                }
                break;

            case R.id.menu_detail_show_contact:
                Intent contactIntent = new Intent(Intent.ACTION_VIEW, mContactUri);
                contactIntent.putExtra("finishActivityOnSaveCompleted", true);
                startActivity(contactIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.contact_detail_menu, menu);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                mContactUri,
                ContactDetailQuery.PROJECTION,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, final Cursor data) {
        if (mContactUri == null) {
            return;
        }

        if (data.moveToFirst()) {
            mContactID = data.getString(ContactDetailQuery.ID);
            mContactName = data.getString(ContactDetailQuery.DISPLAY_NAME);
            getActivity().setTitle(mContactName);

            Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, data.getLong(ContactDetailQuery.ID));
            Uri dataUri = Uri.withAppendedPath(contactUri, Contacts.Data.CONTENT_DIRECTORY);
            Cursor nameCursor = getActivity().getContentResolver().query(
                    dataUri,
                    null,
                    ContactsContract.Data.MIMETYPE+"=?",
                    new String[]{ ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE },
                    null);
            while (nameCursor.moveToNext()) {
                String lastName = nameCursor.getString(nameCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                if (lastName != null && !lastName.isEmpty()) mContactName = lastName;
            }
            nameCursor.close();

            new Handler().post(new Runnable() {
                @Override
                public void run() {
//                    DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getActivity().getApplicationContext());
//                    databaseHelper.open();

                    // Get Notice
                    mNoticeAdapter = new NoticeAdapter(
                            getActivity().getApplicationContext(),
                            databaseHelper.getNotice(mContactID)
                    );

                    // Get Events
                    ContentResolver cr = getActivity().getApplication().getContentResolver();
                    Cursor attendeeCursor = databaseHelper.getEvents(mContactID);
                    ArrayList<String> eventIDs = new ArrayList<String>();
                    while (attendeeCursor.moveToNext()) {
                        eventIDs.add(attendeeCursor.getString(2));
                    }
                    attendeeCursor.close();

                    if (!eventIDs.isEmpty()) {
                        Cursor eventCursor = cr.query(
                                CalendarContract.Events.CONTENT_URI,
                                CalenderAdapter.EventQuery.PROJECTION,
                                Events._ID + " IN (" + makePlaceholders(eventIDs.size()) + ")",
                                eventIDs.toArray(new String[eventIDs.size()]),
                                CalenderAdapter.EventQuery.SORT);

                        while (eventCursor.moveToNext()) {
                            String id = eventCursor.getString(CalenderAdapter.EventQuery.ID);
                            if (eventIDs.contains(id)) eventIDs.remove(id);
                        }

                        for (String id : eventIDs)
                            databaseHelper.deleteEventsByEventId(id);

                        mCalenderAdapter = new CalenderAdapter(
                                getActivity().getApplicationContext(),
                                eventCursor
                        );
                        mCalenderAdapter.setContactName(mContactName);
                    }
                    setMode(selectedMode);
                }
            });
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Nothing to do here. The Cursor does not need to be released as it was never directly
        // bound to anything (like an adapter).
    }

    @Override
    public void onPause() {
        DatabaseHelper.getInstance(getActivity()).close();
        super.onPause();
    }

    /**
     * This interface defines constants used by contact retrieval queries.
     */
    public interface ContactDetailQuery {
        final static String[] PROJECTION = {
                Contacts._ID,
                Contacts.DISPLAY_NAME_PRIMARY
        };

        final static int ID = 0;
        final static int DISPLAY_NAME = 1;
    }

    /**
     * click listener for both ListView
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch(selectedMode) {
            case MODE_NOTICE:
                Cursor mNoticeAdapterCursor = mNoticeAdapter.getCursor();
                mNoticeAdapterCursor.moveToPosition(i);

                TextView textView = (TextView) view.findViewById(R.id.detailNoticeText);

                Bundle bundle = new Bundle();
                bundle.putString(NoticeDialog.EDIT_TEXT_ID, mNoticeAdapterCursor.getString(mNoticeAdapterCursor.getColumnIndex("_id")));
                bundle.putString(NoticeDialog.EDIT_TEXT, textView.getText().toString());

                NoticeDialog noticeDialog = new NoticeDialog();
                noticeDialog.setTargetFragment(this, 0);
                noticeDialog.setArguments(bundle);
                noticeDialog.show(getFragmentManager(),"noticeDialog");
                break;

            case MODE_CALENDAR:
                Cursor mCalenderAdapterCursor = mCalenderAdapter.getCursor();
                mCalenderAdapterCursor.moveToPosition(i);

                Uri uri = ContentUris.withAppendedId(Events.CONTENT_URI, mCalenderAdapterCursor.getLong(CalenderAdapter.EventQuery.ID));
                Intent calendarIntent = new Intent(Intent.ACTION_VIEW)
                        .setData(uri);
                startActivity(calendarIntent);
                break;
        }
    }

    /**
     * Notice callback
     *
     * @param bundle
     */
    @Override
    public void onNoticeDialogCallback(Bundle bundle) {
//        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getActivity().getApplicationContext());
//        databaseHelper.open();

        if (bundle.containsKey(NoticeDialog.ACTION)) {
            switch (bundle.getInt(NoticeDialog.ACTION)) {
                case NoticeDialog.ACTION_ADD:
                    databaseHelper.insertNotice(mContactID, bundle.getString(NoticeDialog.EDIT_TEXT));
                    break;

                case NoticeDialog.ACTION_EDIT:
                    databaseHelper.updateNotice(bundle.getString(NoticeDialog.EDIT_TEXT_ID), bundle.getString(NoticeDialog.EDIT_TEXT));
                    break;

                case NoticeDialog.ACTION_DELETE:
                    databaseHelper.deleteNotice(bundle.getString(NoticeDialog.EDIT_TEXT_ID));
                    break;
            }
            mNoticeAdapter.changeCursor(databaseHelper.getNotice(mContactID));
            mNoticeAdapter.notifyDataSetChanged();
        }
    }

    private void setMode(int mode) {
        selectedMode = mode;
        switch (selectedMode) {
            case MODE_CALENDAR:
                mDetailListViewTitle.setText(R.string.detail_calendar_title);
                ((TextView)mDetailListView.getEmptyView()).setText(R.string.detail_calendar_list_view_empty);
                mDetailListView.setAdapter(mCalenderAdapter);
                break;
            case MODE_NOTICE:
                mDetailListViewTitle.setText(R.string.detail_notice_title);
                ((TextView)mDetailListView.getEmptyView()).setText(R.string.detail_notice_list_view_empty);
                mDetailListView.setAdapter(mNoticeAdapter);
                break;
        }
    }

    private String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }
}
