package de.memoria.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import de.memoria.app.R;
import de.memoria.app.adapter.DialDialogAdapter;

import static android.provider.ContactsContract.CommonDataKinds.Phone;

/**
 * Created by maikel on 30.10.14.
 */

public class DialDialog extends DialogFragment {

    public static final String CONTACT_ID = "_CONTACT_ID";
    public static final String CONTACT_NAME = "_CONTACT_NAME";
    public static final String CONTACT_URI = "_CONTACT_URI";
    public static final int DIALRESULT = 0;
    private String contactID;
    private String contactName;
    private String contactUri;

    /**
     * Implement this interface to receive a bundle
     * that contains the ACTION and the TEXT
     */
    public interface DialDialogListener {
        public void onDialDialogCallback(DialDialog dialDialog, String contactID, String contactName, String contactUri);
    }

    DialDialogListener callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.memoriaDialog);
        try {
            if(callback == null)
                callback = (DialDialogListener) getTargetFragment();
        } catch (Exception e) {
            throw new ClassCastException("Calling Fragment must implement onDialDialogCallback");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        callback = (DialDialogListener) getTargetFragment();

        final Bundle bundle = new Bundle();

        LayoutInflater inflater = getActivity().getLayoutInflater();

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.memoriaDialog));
        builder.setTitle(R.string.dialog_dial_title);
        View view = inflater.inflate(R.layout.dial_dialog_list, null);
        ListView listView = (ListView) view.findViewById(android.R.id.list);
        listView.setEmptyView(view.findViewById(android.R.id.empty));
        builder.setView(view);

        if(getArguments() != null) {
            bundle.putAll(getArguments());
            contactID = bundle.getString(CONTACT_ID);
            contactName = bundle.getString(CONTACT_NAME);
            contactUri = bundle.getString(CONTACT_URI);

            Cursor cursorPhone = getActivity().getContentResolver().query(
                    Phone.CONTENT_URI,
                    PHONE_PROJECTION,
                    Phone.CONTACT_ID + " = ? ",
                    new String[]{contactID},
                    null);

            DialDialogAdapter mAdapter = new DialDialogAdapter(
                    getActivity().getApplicationContext(),
                    cursorPhone
            );

            listView.setAdapter(mAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    TextView textView = (TextView) view.findViewById(R.id.dialDialogPhoneNumber);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + textView.getText().toString()));
                    startActivityForResult(intent, DIALRESULT);
                }
            });
        }

        builder.setNegativeButton(R.string.dialog_generel_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DialDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DIALRESULT) {
            callback.onDialDialogCallback(this, contactID, contactName, contactUri);
        }
    }

    private static final String[] PHONE_PROJECTION = new String[] {
            Phone._ID,
            Phone.NUMBER,
            Phone.TYPE
    };
}
