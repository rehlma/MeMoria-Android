package de.memoria.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import de.memoria.app.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by maikel on 30.10.14.
 */

public class ReminderDialog extends DialogFragment {

    public static final String REMINDER_ID = "_REMINDER_ID";
    public static final String REMINDER_TITLE = "_REMINDER_TITLE";
    public static final String REMINDER_DATE = "_REMINDER_DATE";
    public static final String ACTION = "_ACTION";
    public static final int ACTION_ADD = 0;
    public static final String CONTACT_NAME = "_CONTACT_NAME";

    /**
     * Implement this interface to receive a bundle
     * that contains the ACTION and the TEXT
     */
    public interface ReminderDialogListener {
        public void onReminderDialogCallback(ReminderDialog reminderDialog, Bundle bundle);
    }

    public void setReminderDialogListener(ReminderDialogListener callback) {
        this.callback = callback;
    }

    ReminderDialogListener callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.memoriaDialog);
        try {
            if(callback == null)
                callback = (ReminderDialogListener) getTargetFragment();
        } catch (Exception e) {
            throw new ClassCastException("Calling Fragment must implement onReminderDialogCallback");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final EditText editText = new EditText(getActivity());
        int dp = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        editText.setPadding(dp, dp, dp, dp);
        editText.setSingleLine();
        editText.setHint("Titel");
        final DatePicker datePicker = new DatePicker(getActivity());
        final TimePicker timePicker = new TimePicker(getActivity());
        timePicker.setIs24HourView(true);
        datePicker.setCalendarViewShown(false);

        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(editText);
        linearLayout.addView(datePicker);
        linearLayout.addView(timePicker);

        final Bundle putBundle = new Bundle();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(linearLayout);
        builder.setTitle(R.string.dialog_generel_add);

        if(getArguments() != null) {
            Bundle getBundle = new Bundle();
            getBundle.putAll(getArguments());

            if (getBundle.containsKey(CONTACT_NAME)) {
                editText.setText(getResources().getString(R.string.dialog_reminder_default_text, getBundle.getString(CONTACT_NAME)));
            }
            else if(getBundle.containsKey(REMINDER_TITLE)) {
                editText.setText(getBundle.getString(REMINDER_TITLE));
            }
        }

        builder.setPositiveButton(R.string.dialog_generel_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String text = editText.getText().toString();

                if (text.trim().length() == 0) {
                    dismiss();
                } else {
                    Calendar calendar = Calendar.getInstance(Locale.GERMANY);
                    calendar.set(Calendar.YEAR, datePicker.getYear());
                    calendar.set(Calendar.MONTH, datePicker.getMonth());
                    calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                    calendar.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                    calendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());
                    calendar.set(Calendar.SECOND, 0);

                    String date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")).format(calendar.getTimeInMillis());
                    putBundle.putInt(ACTION, ACTION_ADD);
                    putBundle.putString(REMINDER_TITLE, text);
                    putBundle.putString(REMINDER_DATE, date);

                    callback.onReminderDialogCallback(ReminderDialog.this, putBundle);
                }
            }
        });
        builder.setNegativeButton(R.string.dialog_generel_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                    ReminderDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }
}
