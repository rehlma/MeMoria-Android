package de.memoria.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by maikel on 30.10.14.
 */
public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "memoria.db";

    public static final String NOTICE_TABLE_NAME = "notice";         // Table name
    public static final String NOTICE_TABLE_COLUMN_ID = "_id";     // a column named "_id" is required for cursor
    public static final String NOTICE_TABLE_COLUMN_CONTACT = "contact";
    public static final String NOTICE_TABLE_COLUMN_TEXT = "text";
    public static final String NOTICE_TABLE_COLUMN_CREATE = "created";

    public static final String REMINDER_TABLE_NAME = "reminder";       // Table name
    public static final String REMINDER_TABLE_COLUMN_ID = "_id";
    public static final String REMINDER_TABLE_COLUMN_TITLE = "title";
    public static final String REMINDER_TABLE_COLUMN_DATE = "date";
    public static final String REMINDER_TABLE_COLUMN_STATUS = "status";

    public static final String FAV_TABLE_NAME = "favorite";
    public static final String FAV_TABLE_COLUMN_ID = "_id";

    public static final String EVENTS_TABLE_NAME = "events";
    public static final String EVENTS_TABLE_COLUMN_ID = "_id";
    public static final String EVENTS_TABLE_COLUMN_CONTACT_ID = "contact_id";
    public static final String EVENTS_TABLE_COLUMN_EVENT_ID = "event_id";

    private SQLiteDatabase database;
    private AtomicInteger mOpenCounter = new AtomicInteger();

    private static DatabaseHelper databaseHelper;

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (databaseHelper == null) databaseHelper = new DatabaseHelper(context.getApplicationContext());
        return databaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Create your tables here

        String buildSQL = "CREATE TABLE " + NOTICE_TABLE_NAME +
                "( " +
                NOTICE_TABLE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                NOTICE_TABLE_COLUMN_CONTACT + " TEXT,  " +
                NOTICE_TABLE_COLUMN_TEXT + " TEXT, " +
                NOTICE_TABLE_COLUMN_CREATE + " DATETIME DEFAULT CURRENT_TIMESTAMP " +
                ")";

        Log.d(TAG, "onCreate SQL: " + buildSQL);

        sqLiteDatabase.execSQL(buildSQL);

        buildSQL = "CREATE TABLE " + REMINDER_TABLE_NAME +
                "( " +
                REMINDER_TABLE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                REMINDER_TABLE_COLUMN_TITLE + " TEXT, " +
                REMINDER_TABLE_COLUMN_STATUS + " INTEGER(1) DEFAULT 0, " +
                REMINDER_TABLE_COLUMN_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP " +
                ")";

        Log.d(TAG, "onCreate SQL: " + buildSQL);

        sqLiteDatabase.execSQL(buildSQL);

        buildSQL = "CREATE TABLE " + FAV_TABLE_NAME +
                "( " +
                FAV_TABLE_COLUMN_ID + " INTEGER PRIMARY KEY" +
                ")";

        Log.d(TAG, "onCreate SQL: " + buildSQL);

        sqLiteDatabase.execSQL(buildSQL);

        buildSQL = "CREATE TABLE " + EVENTS_TABLE_NAME +
                "( " +
                EVENTS_TABLE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                EVENTS_TABLE_COLUMN_CONTACT_ID + " INTEGER , " +
                EVENTS_TABLE_COLUMN_EVENT_ID + " INTEGER " +
                ")";

        Log.d(TAG, "onCreate SQL: " + buildSQL);

        sqLiteDatabase.execSQL(buildSQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // Database schema upgrade code goes here

//            String buildSQL = "DROP TABLE IF EXISTS " + NOTICE_TABLE_NAME;
//
//            Log.d(TAG, "onUpgrade SQL: " + buildSQL);
//
//            sqLiteDatabase.execSQL(buildSQL);       // drop previous table
//
//            onCreate(sqLiteDatabase);               // create the table from the beginning
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public synchronized void open(boolean writable) throws SQLException {
        if(mOpenCounter.incrementAndGet() == 1)
            database = writable ? getWritableDatabase() : getReadableDatabase();
    }

    public synchronized boolean isOpen() {
        if (database != null) {
            return database.isOpen();
        }
        else return false;
    }

    public synchronized void close() {
        if(mOpenCounter.decrementAndGet() == 0) close();
    }

    /**
     * NOTICE
     */
    public synchronized void insertNotice (String contact, String text) {
        // we are using ContentValues to avoid sql format errors
        ContentValues contentValues = new ContentValues();

        contentValues.put(NOTICE_TABLE_COLUMN_CONTACT, contact);
        contentValues.put(NOTICE_TABLE_COLUMN_TEXT, text);

        database.insert(NOTICE_TABLE_NAME, null, contentValues);

        Log.d(TAG, "insertNotice SQL:");
    }

    public synchronized void updateNotice (String textId, String text) {

        // we are using ContentValues to avoid sql format errors
        ContentValues contentValues = new ContentValues();

        contentValues.put(NOTICE_TABLE_COLUMN_TEXT, text);

        database.update(NOTICE_TABLE_NAME, contentValues, NOTICE_TABLE_COLUMN_ID + "=" + textId, null);

        Log.d(TAG, "updateNotice SQL:");
    }

    public synchronized void deleteNotice (String textId) {
        database.delete(NOTICE_TABLE_NAME, NOTICE_TABLE_COLUMN_ID + "=" + textId, null);
        Log.d(TAG, "deleteNotice SQL:");
    }

    public synchronized Cursor getNotice (String ID) {

        String buildSQL = "SELECT * FROM " + NOTICE_TABLE_NAME + " WHERE " + NOTICE_TABLE_COLUMN_CONTACT + " = " + ID;

        Log.d(TAG, "getNotice SQL: " + buildSQL);

        return database.rawQuery(buildSQL, null);
    }


    /**
     * REMINDER
     */
    public synchronized void insertReminder (String title, String date) {

        // we are using ContentValues to avoid sql format errors
        ContentValues contentValues = new ContentValues();

        contentValues.put(REMINDER_TABLE_COLUMN_TITLE, title);
        contentValues.put(REMINDER_TABLE_COLUMN_DATE, date);

        database.insert(REMINDER_TABLE_NAME, null, contentValues);

        Log.d(TAG, "insertReminder SQL:");
    }

    public synchronized Cursor getReminder () {

        String buildSQL = "SELECT * FROM " + REMINDER_TABLE_NAME +
                " ORDER BY " + REMINDER_TABLE_COLUMN_DATE + " ASC";

        //Log.d(TAG, "getReminder SQL: " + buildSQL);

        return database.rawQuery(buildSQL, null);
    }

    public synchronized Cursor getReminderNotReaded () {

        String buildSQL = "SELECT * FROM " + REMINDER_TABLE_NAME +
                " WHERE " + REMINDER_TABLE_COLUMN_STATUS + " = 0 " +
                " ORDER BY " + REMINDER_TABLE_COLUMN_DATE + " ASC";

        Log.d(TAG, "getReminderNotReaded SQL: " + buildSQL);

        return database.rawQuery(buildSQL, null);
    }

    public synchronized void updateReminder (String id) {

        ContentValues contentValues = new ContentValues();

        contentValues.put(REMINDER_TABLE_COLUMN_STATUS, 1);

        database.update(REMINDER_TABLE_NAME, contentValues, REMINDER_TABLE_COLUMN_ID + "=" + id, null);

        Log.d(TAG, "updateReminder SQL:");
    }

    public synchronized void deleteReminder (String textId) {
        database.delete(REMINDER_TABLE_NAME, REMINDER_TABLE_COLUMN_ID + "=" + textId, null);
        Log.d(TAG, "deleteReminder SQL:");
    }

    /**
     * FAVORITE
     */
    public synchronized void insertFavorite (long id) {

        // we are using ContentValues to avoid sql format errors
        ContentValues contentValues = new ContentValues();

        contentValues.put(FAV_TABLE_COLUMN_ID, id);

        database.insert(FAV_TABLE_NAME, null, contentValues);

        Log.d(TAG, "insertFavorite SQL:");
    }

    public synchronized void deleteFavorite (long id) {
        database.delete(FAV_TABLE_NAME, FAV_TABLE_COLUMN_ID + "=" + id, null);
        Log.d(TAG, "deleteFavorite SQL:");
    }

    public synchronized Cursor getFavorites () {

        String buildSQL = "SELECT * FROM " + FAV_TABLE_NAME;

        Log.d(TAG, "getFavorites SQL: " + buildSQL);

        return database.rawQuery(buildSQL, null);
    }

    /**
     * EVENTS
     */
    public synchronized void insertEvents (String contactId, int eventId) {

        // we are using ContentValues to avoid sql format errors
        ContentValues contentValues = new ContentValues();

        contentValues.put(EVENTS_TABLE_COLUMN_CONTACT_ID, contactId);
        contentValues.put(EVENTS_TABLE_COLUMN_EVENT_ID, eventId);

        database.insert(EVENTS_TABLE_NAME, null, contentValues);

        Log.d(TAG, "insertEvents SQL:");
    }

    public synchronized void deleteEventsByContactId (String contactId) {
        database.delete(EVENTS_TABLE_NAME, EVENTS_TABLE_COLUMN_CONTACT_ID + "=" + contactId, null);
        Log.d(TAG, "deleteEventsByContactId SQL:" + contactId);
    }

    public synchronized void deleteEventsByEventId (String eventId) {
        database.delete(EVENTS_TABLE_NAME, EVENTS_TABLE_COLUMN_EVENT_ID + "=" + eventId, null);
        Log.d(TAG, "deleteEventsByEventId SQL:" + eventId);
    }

    public synchronized Cursor getEvents (String contactId) {

        String buildSQL = "SELECT * FROM " + EVENTS_TABLE_NAME + " WHERE " + EVENTS_TABLE_COLUMN_CONTACT_ID + " = " + contactId;

        Log.d(TAG, "getEvents SQL: " + buildSQL);

        return database.rawQuery(buildSQL, null);
    }
}
