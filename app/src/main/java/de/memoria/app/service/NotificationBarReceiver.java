package de.memoria.app.service;

/**
 * Created by maikel on 02.11.14.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import de.memoria.app.R;
import de.memoria.app.database.DatabaseHelper;
import de.memoria.app.ui.ReminderActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NotificationBarReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        //Log.d("NotificationAlarm", "onReceive");

        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        databaseHelper.open(false);
        Cursor cursor = databaseHelper.getReminderNotReaded();

        while (cursor.moveToNext()) {
            try {
                String dateString = cursor.getString(cursor.getColumnIndex(DatabaseHelper.REMINDER_TABLE_COLUMN_DATE));
                String title = cursor.getString(cursor.getColumnIndex(DatabaseHelper.REMINDER_TABLE_COLUMN_TITLE));
                Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")).parse(dateString);

                if (date.getTime() < Calendar.getInstance().getTimeInMillis()) {

                    NotificationManager notifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                    Intent notificationIntent = new Intent(context, ReminderActivity.class);
                    notificationIntent.putExtra(ReminderActivity.FROM_NOTIFICATION, true);
                    notificationIntent.putExtra(ReminderActivity.FROM_NOTIFICATION_TITLE, title);

                    PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_notification)
                                    .setContentText(title)
                                    .setContentTitle(context.getString(R.string.app_name))
                                    .setTicker(context.getString(R.string.notification_popup))
                                    .setAutoCancel(true)
                                    .setContentIntent(contentIntent);

                    notifyManager.notify(1, mBuilder.build());

                    Log.d("Notification",cursor.getString(cursor.getColumnIndex(DatabaseHelper.REMINDER_TABLE_COLUMN_ID)));
                    databaseHelper.updateReminder(cursor.getString(cursor.getColumnIndex(DatabaseHelper.REMINDER_TABLE_COLUMN_ID)));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        databaseHelper.close();
    }
}